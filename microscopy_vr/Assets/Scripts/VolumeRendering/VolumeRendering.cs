﻿
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;
//using UnityEditor;

namespace VolumeRendering
{

    [RequireComponent (typeof(MeshRenderer), typeof(MeshFilter))]
    public class VolumeRendering : MonoBehaviour {

        // public GameObject cubeContainer;
	      public GameObject cube;

        [SerializeField] protected Shader shader;
        protected Material material;

        [SerializeField] Color color = Color.white;
        [Range(0f, 1f)] public int channel = 0;
        [Range(0f, 1f)] public int c1 = 1;
        [Range(0f, 1f)] public int c2 = 1;
        [Range(0f, 1f)] public int c3 = 1;
        [Range(0, 10)] public int timepoint = 0;
        [Range(0f, 1f)] public float threshold = 0.5f;
      	[Range(0f, 1f)] public float rotx = 0.0f;
      	[Range(0f, 1f)] public float roty = 0.0f;
        [Range(0f, 1f)] public float rotz = 0.25f;
      	[Range(0.01f, 2f)] public float rate = 0.33f;
        [Range(1f, 10f)] public float intensity = 1.5f;
        [Range(0f, 2f)] public float scale = 1.0f;
        [Range(0f, 1f)] public float sliceXMin = 0.0f, sliceXMax = 1.0f;
        [Range(0f, 1f)] public float sliceYMin = 0.0f, sliceYMax = 1.0f;
        [Range(0f, 1f)] public float sliceZMin = 0.0f, sliceZMax = 1.0f;

        public bool play = false;
        public int maxtimepoint = 10;
        private float lastTime = 0.0F;

        [SerializeField] protected Texture3D volume;

        public VolumeSettings settings;
        public Texture3D[,] Volumes;
        public Vector3 DropPoint;
        public GameObject Container;
        public GameObject Spinner;
        public VolumeDatabase Database;
        public int DatasetID;

        protected void Start () {

            //DropPoint = ...
            //Container = new GameObject("VolumeContainer_" + Database.Datasets[DatasetID]);

            // Data = Database.Database[DatasetID]

            material = new Material(shader);
            GetComponent<MeshFilter>().sharedMesh = Build();
            GetComponent<MeshRenderer>().sharedMaterial = material;

            settings = Database.Settings[DatasetID];
            maxtimepoint = settings.timepoints;
            // Volumes = new Texture3D[settings.channels, settings.timepoints+1];

            // string tex = String.Format("c{0}_t{1:D5}.raw", 0, 0);
            // string path = Path.Combine(Application.dataPath, Path.Combine(DataPath , tex));
            // volume = LoadRAW(path) as Texture3D;
            // material.SetTexture("_Volume", volume);
            // material.SetTexture("_Volume", volume);
	          // cube = GameObject.Find("VolumeRendering");

            lastTime = Time.time;

            StartCoroutine("Wait");
        }

        IEnumerator Wait() {
          // Loading the TimeLapse into RAM for fluidity!
          while (!Database.Loaded[DatasetID]) {
              Spinner.GetComponent<SpinnerController>().LoadingBar.value = Database.Database[DatasetID].Progress;
              yield return null;
            }

          Volumes = Database.Datas[DatasetID];
          Spinner.SetActive(false);
          cube.SetActive(true);
          Scale(1.0f);
          print("Changing Texture");
          changeTexture();
        }

        public void Scale(float f) {
            scale = f;
            print(settings.x);
            f = 100 / f;
            float sx = settings.x/f;
            float sy = settings.y/f;
            float sz = settings.z/f;
            // cubeContainer.transform.localScale = new Vector3(sx+0.1f, sy+0.1f, sz+0.1f);
            cube.transform.localScale = new Vector3(sx, sy, sz);
        }

        private static VolumeSettings loadTex3DSettings(string path)
        {
          string filePath = Path.Combine(Application.dataPath, Path.Combine(path , "settings.json"));
          print(filePath);
          // Read the json from the file into a string
          string dataAsJson = File.ReadAllText(filePath);
          // Pass the json to JsonUtility, and tell it to create a GameData object from it
          VolumeSettings settings = VolumeSettings.CreateFromJSON(dataAsJson);
          return settings;

        }

        public void changeTexture() {
          volume = Volumes[channel, timepoint];
          material.SetTexture("_Volume", volume);
          material.SetInt("_Channels", settings.channels);
        }

        public void changeSlice() {
          material.SetVector("_SliceMin", new Vector3(sliceXMin, sliceYMin, sliceZMin));
          material.SetVector("_SliceMax", new Vector3(sliceXMax, sliceYMax, sliceZMax));
        }

        public void changeThreshold() {
          material.SetFloat("_Threshold", threshold);
        }

        public void changeIntensity() {
          material.SetFloat("_Intensity", intensity);
        }

        public void changeColor() {
          material.SetColor("_Color", color);
        }

        public void rotate() {
          cube.transform.eulerAngles = new Vector3(rotx * 360, roty * 360, rotz * 360);
        }

        public void changeChannels() {
          material.SetInt("_C1", c1);
          material.SetInt("_C2", c2);
          material.SetInt("_C3", c3);
        }

        protected void Update () {

            if (play) {
              var newtime = Time.time;
              if (newtime - lastTime > rate) {
                timepoint += 1;
                if (timepoint > maxtimepoint) {
                  timepoint = 0;
                }
                changeTexture();
                lastTime = Time.time;
              }
          }
        }


        Mesh Build() {
            var vertices = new Vector3[] {
                new Vector3 (-0.5f, -0.5f, -0.5f),
                new Vector3 ( 0.5f, -0.5f, -0.5f),
                new Vector3 ( 0.5f,  0.5f, -0.5f),
                new Vector3 (-0.5f,  0.5f, -0.5f),
                new Vector3 (-0.5f,  0.5f,  0.5f),
                new Vector3 ( 0.5f,  0.5f,  0.5f),
                new Vector3 ( 0.5f, -0.5f,  0.5f),
                new Vector3 (-0.5f, -0.5f,  0.5f),
            };
            var triangles = new int[] {
                0, 2, 1,
                0, 3, 2,
                2, 3, 4,
                2, 4, 5,
                1, 2, 5,
                1, 5, 6,
                0, 7, 4,
                0, 4, 3,
                5, 4, 7,
                5, 7, 6,
                0, 6, 7,
                0, 1, 6
            };

            var mesh = new Mesh();
            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.RecalculateNormals();
            return mesh;
        }

        void OnValidate()
        {
            Constrain(ref sliceXMin, ref sliceXMax);
            Constrain(ref sliceYMin, ref sliceYMax);
            Constrain(ref sliceZMin, ref sliceZMax);
        }

        void Constrain (ref float min, ref float max)
        {
            const float threshold = 0.025f;
            if(min > max - threshold)
            {
                min = max - threshold;
            } else if(max < min + threshold)
            {
                max = min + threshold;
            }
        }

        void OnDestroy()
        {
            Destroy(material);
        }

    }

}
