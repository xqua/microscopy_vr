// loads the raw binary data into a texture saved as a Unity asset
// (so can be de-activated after a given data cube has been converted)
// adapted from a XNA project by Kyle Hayward
// http://graphicsrunner.blogspot.ca/2009/01/volume-rendering-101.html
// data can be UInt8 or Float32, expected to be normalized in [0, 1]
// Gilles Ferrand, University of Manitoba / RIKEN, 2016–2017

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.IO; // to get BinaryReader
using System.Linq; // to get array's Min/Max
using System.Runtime.InteropServices;
using System.Collections;
using System.Collections.Generic;
using VRTK;


namespace VolumeRendering
{

    [RequireComponent (typeof(MeshRenderer), typeof(MeshFilter))]
    public class VolumeRenderingNew : MonoBehaviour {

			// [SerializeField] protected Shader shader;
			protected Material material;

			public Color color = Color.white;
			[Range(0f, 1f)] public int channel = 0;
      [Range(0, 10)] public int timepoint = 0;
      [Range(1f, 1024f)] public float Quality = 256f;
      [Range(0.1f, 3f)] public float contrast = 1f;
      [Range(0f, 1f)] public float thresholdMin = 0.0f;
			[Range(0f, 1f)] public float thresholdMax = 1f;
			[Range(0f, 1f)] public float rotx = 0;
			[Range(0f, 1f)] public float roty = 0;
			[Range(0f, 1f)] public float rotz = 0;
      [Range(0f, 1f)] public float lastrotx;
      [Range(0f, 1f)] public float lastroty;
      [Range(0f, 1f)] public float lastrotz;
			[Range(0.01f, 2f)] public float rate = 0.33f;
			[Range(1f, 10f)] public float intensity = 1.5f;
			[Range(0f, 2f)] public float scale = 1.0f;
			[Range(0f, 1f)] public float sliceXMin = 0.0f, sliceXMax = 1.0f;
			[Range(0f, 1f)] public float sliceYMin = 0.0f, sliceYMax = 1.0f;
			[Range(0f, 1f)] public float sliceZMin = 0.0f, sliceZMax = 1.0f;

			public bool play = false;
      public bool c1 = true;
      public bool c2 = true;
      public bool c3 = true;

      public int maxtimepoint = 10;
			public int maxChannel = 1;
			private float lastTime = 0.0F;

			[SerializeField] protected Texture3D volume;

			public VolumeSettings settings;
			public Texture3D[,] Volumes;
			public Vector3 DropPoint;
			public GameObject Container;
      public GameObject Spinner;
			public GameObject Cursor;
			public VolumeDatabase Database;
      public GameObject cube;
      public int DatasetID;
      public int DatabaseVolumeID;
      public bool Loaded;

      // public float colorR = 1;
      // public float colorG = 1;
      // public float colorB = 1;

			void Start() {

				material = GetComponent<Renderer> ().material;
				Vector4 channels = new Vector4 (0, 0, 0, 1);
				material.SetVector ("_DataChannel", channels);

				settings = Database.Settings[DatasetID];
        maxtimepoint = settings.timepoints - 1;
				maxChannel = settings.channels;

				lastTime = Time.time;

				StartCoroutine("Wait");

			}

			IEnumerator Wait() {
				// Loading the TimeLapse into RAM for fluidity!
				while (!Database.Loaded[DatasetID]) {
						Spinner.GetComponent<SpinnerController>().LoadingBar.value = Database.Database[DatasetID].Progress;
						yield return null;
					}

				Volumes = Database.Datas[DatasetID];
				Spinner.SetActive(false);
				cube.SetActive(true);
        Loaded = true;
				Scale(1.0f);
				print("Changing Texture");
				changeTexture();
			}

      public void SetSelected(bool selected) {
        if (selected) {
          Cursor.SetActive(true);
        }
        else {
          Cursor.SetActive(false);
        }
      }

		  public void Scale(float f) {
		      scale = f;
		      // print(settings.x);
		      f = 100 / f;
		      float sx = settings.x/f;
		      float sy = settings.y/f;
		      float sz = settings.z/f;
		      // cubeContainer.transform.localScale = new Vector3(sx+0.1f, sy+0.1f, sz+0.1f);
		      cube.transform.localScale = new Vector3(sx, sy, sz);
		  }

		  private static VolumeSettings loadTex3DSettings(string path)
		  {
		    string filePath = Path.Combine(Application.dataPath, Path.Combine(path , "settings.json"));
		    print(filePath);
		    // Read the json from the file into a string
		    string dataAsJson = File.ReadAllText(filePath);
		    // Pass the json to JsonUtility, and tell it to create a GameData object from it
		    VolumeSettings settings = VolumeSettings.CreateFromJSON(dataAsJson);
		    return settings;

		  }

		  public void changeTexture() {
		    volume = Volumes[channel, timepoint];
		    material.SetTexture("_Data", volume);
		  }

      public void Rotate() {
        lastrotx = rotx;
        lastroty = roty;
        lastrotz = rotz;
        cube.transform.eulerAngles = new Vector3(rotx, roty, rotz);
      }

		  protected void Update () {
          if (cube.GetComponent<VRTK_InteractableObject>().IsGrabbed()) {
            if (cube.transform.eulerAngles.x != lastrotx) {
              rotx = cube.transform.eulerAngles.x;
              lastrotx = rotx;
              // print("triggered");
            }
            if (cube.transform.eulerAngles.y != lastroty) {
              roty = cube.transform.eulerAngles.y;
              lastroty = roty;
            }
            if (cube.transform.eulerAngles.z != lastrotz) {
              rotz = cube.transform.eulerAngles.z;
              lastrotz = rotz;
            }
          }

		      material.SetColor("_Color", color);
          material.SetFloat("_Steps", Quality);
          material.SetFloat("_DataMin", thresholdMin);
          material.SetFloat("_DataMax", thresholdMax);
          material.SetFloat("_StretchPower", contrast);
		      material.SetFloat("_NormPerRay", intensity);
          material.SetFloat("_SliceAxis1Min", sliceXMin);
          material.SetFloat("_SliceAxis2Min", sliceYMin);
          material.SetFloat("_SliceAxis3Min", sliceZMin);
          material.SetFloat("_SliceAxis1Max", sliceXMax);
          material.SetFloat("_SliceAxis2Max", sliceYMax);
		      material.SetFloat("_SliceAxis3Max", sliceZMax);
          // material.SetInt("_")
          // material.SetColor("_")
		      // material.SetVector("_SliceMin", new Vector3(sliceXMin, sliceYMin, sliceZMin));
		      // material.SetVector("_SliceMax", new Vector3(sliceXMax, sliceYMax, sliceZMax));
		      if (play) {
		        var newtime = Time.time;
		        if (newtime - lastTime > rate) {
		          timepoint += 1;
		          if (timepoint > maxtimepoint) {
		            timepoint = 0;
		          }
		          changeTexture();
		          lastTime = Time.time;
		        }
		    }
		  }

			void OnDestroy()
			{
					Destroy(material);
			}

	}

}
