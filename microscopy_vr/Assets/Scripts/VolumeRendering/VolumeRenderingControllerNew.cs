  ﻿using System.Collections;
  using System.Collections.Generic;

  using UnityEngine;
  using UnityEngine.UI;

  using TMPro;

  using VRTK;

  namespace VolumeRendering
  {

      public class VolumeRenderingControllerNew : MonoBehaviour {

          public VolumeRenderingNew volume;
          public VolumeDatabase Database;
          public int currentVolumeIndex;
          [SerializeField] protected Slider sliderXMin, sliderXMax, sliderXSize;
          [SerializeField] protected Slider sliderYMin, sliderYMax, sliderYSize;
          [SerializeField] protected Slider sliderZMin, sliderZMax, sliderZSize;
          [SerializeField] protected Scrollbar scrollbarXCenter, scrollbarYCenter, scrollbarZCenter;
          [SerializeField] protected Slider sliderRotX, sliderRotY, sliderRotZ, sliderScale;
          [SerializeField] protected Slider sliderThresholdMin, sliderThresholdMax, sliderThresholdSize;
          [SerializeField] protected Scrollbar scrollbarThresholdCenter;
          [SerializeField] protected Slider sliderQuality, sliderIntensity, sliderContrast;
          [SerializeField] protected Slider sliderTimepoint, sliderRate;
          [SerializeField] protected Slider sliderColorR, sliderColorG, sliderColorB;
          [SerializeField] protected GameObject ImageColor;
          [SerializeField] protected Dropdown dropChannel;
          [SerializeField] protected GameObject Channels;
          [SerializeField] protected Toggle Channel1;
          [SerializeField] protected Toggle Channel2;
          [SerializeField] protected Toggle Channel3;
          [SerializeField] protected TMP_Text TextTimepoint;
          [SerializeField] protected GameObject Container;

          const float sliceThreshold = 0.025f;

          static Toggle.ToggleEvent emptyToggleEvent = new Toggle.ToggleEvent();
          static Slider.SliderEvent emptySliderEvent = new Slider.SliderEvent();
          static Scrollbar.ScrollEvent emptyScrollbarEvent = new Scrollbar.ScrollEvent();



        void Start ()
        {

            // sliderXMin.onValueChanged.AddListener(delegate {OnSliceXMinChange(); });

            sliderXMin.onValueChanged.AddListener(delegate {OnSliceXMinChange(); });
            sliderXMax.onValueChanged.AddListener(delegate {OnSliceXMaxChange();});
            sliderXSize.onValueChanged.AddListener(delegate {OnSliceXSizeChange();});
            scrollbarXCenter.onValueChanged.AddListener(delegate {OnSliceXCenterChange();});

            sliderYMin.onValueChanged.AddListener(delegate {OnSliceYMinChange();});
            sliderYMax.onValueChanged.AddListener(delegate {OnSliceYMaxChange();});
            sliderYSize.onValueChanged.AddListener(delegate {OnSliceYSizeChange();});
            scrollbarYCenter.onValueChanged.AddListener(delegate {OnSliceYCenterChange();});

            sliderZMin.onValueChanged.AddListener(delegate {OnSliceZMinChange();});
            sliderZMax.onValueChanged.AddListener(delegate {OnSliceZMaxChange();});
            sliderZSize.onValueChanged.AddListener(delegate {OnSliceZSizeChange();});
            scrollbarZCenter.onValueChanged.AddListener(delegate {OnSliceZCenterChange();});

            sliderThresholdMin.onValueChanged.AddListener(delegate {OnThresholdMinChange();});
            sliderThresholdMax.onValueChanged.AddListener(delegate {OnThresholdMaxChange();});
            sliderThresholdSize.onValueChanged.AddListener(delegate {OnThresholdSizeChange();});
            scrollbarThresholdCenter.onValueChanged.AddListener(delegate {OnThresholdCenterChange();});

            // dropChannel.onValueChanged.AddListener(delegate {OnChannelChanged();});

            Channel1.onValueChanged.AddListener(delegate {OnChannelChanged(1);});
            Channel2.onValueChanged.AddListener(delegate {OnChannelChanged(2);});
            Channel3.onValueChanged.AddListener(delegate {OnChannelChanged(3);});

            sliderRotX.onValueChanged.AddListener(delegate {OnRotationXChanged();});
            sliderRotY.onValueChanged.AddListener(delegate {OnRotationYChanged();});
            sliderRotZ.onValueChanged.AddListener(delegate {OnRotationZChanged();});

            sliderColorR.onValueChanged.AddListener(delegate {OnColorRChange();});
            sliderColorG.onValueChanged.AddListener(delegate {OnColorGChange();});
            sliderColorB.onValueChanged.AddListener(delegate {OnColorBChange();});

            sliderScale.onValueChanged.AddListener(delegate {OnScaleChanged();});

            sliderQuality.onValueChanged.AddListener(delegate {OnQualityChanged();});

            sliderContrast.onValueChanged.AddListener(delegate {OnContrastChanged();});

            sliderIntensity.onValueChanged.AddListener(delegate {OnIntensityChanged();});

            sliderTimepoint.onValueChanged.AddListener(delegate {OnTimepointChanged();});

            sliderRate.onValueChanged.AddListener(delegate {OnRateChanged();});

            print("Events Set !");
        }

        void Update() {
          if (volume) {
            TextTimepoint.text = string.Format("{0}",volume.timepoint);
            sliderTimepoint.value = volume.timepoint;
            if (volume.GetComponent<VRTK_InteractableObject>().IsGrabbed()) {
              sliderRotX.value = volume.rotx;
              sliderRotY.value = volume.roty;
              sliderRotZ.value = volume.rotz;
            }
          }
        }

        public void SetVolume(int index) {
          currentVolumeIndex = index;
          Container = Database.VolumeContainers[index];
          volume = Container.GetComponentInChildren<VolumeRenderingNew>();
          print("Setting Volume on Menu Contoller");
          sliderXMin.value = volume.sliceXMin;
          sliderXMax.value = volume.sliceXMax;
          sliderYMax.value = volume.sliceYMax;
          sliderYMin.value = volume.sliceYMin;
          sliderZMax.value = volume.sliceZMax;
          sliderZMin.value = volume.sliceZMin;
          if (volume.maxChannel == 1) {
            print("Volume Channel 1");
            Channel1.interactable = false;
            Channel2.interactable = false;
            Channel3.interactable = false;
            ToggleSetValue(Channel1, true);
            ToggleSetValue(Channel2, false);
            ToggleSetValue(Channel3, false);
          }
          if (volume.maxChannel == 2) {
            Channel1.interactable = true;
            Channel2.interactable = true;
            Channel3.interactable = false;
            ToggleSetValue(Channel1, true);
            ToggleSetValue(Channel2, true);
            ToggleSetValue(Channel3, false);
          }
          if (volume.maxChannel == 3) {
            Channel1.interactable = true;
            Channel2.interactable = true;
            Channel3.interactable = true;
            ToggleSetValue(Channel1, true);
            ToggleSetValue(Channel2, true);
            ToggleSetValue(Channel3, true);
          }
          // ToggleSetValue(Channel1, true);
          // ToggleSetValue(Channel2, false);
          // ToggleSetValue(Channel3, false);

          //   Channels.SetActive(true);
          //   dropChannel.ClearOptions();
          //   List<string> DropOptions = new List<string>();
          //   for (int i =0 ; i < volume.maxChannel; i++) {
          //     DropOptions.Add(string.Format("Channel {0}", i));
          //   }
          //   dropChannel.AddOptions(DropOptions);
          //   dropChannel.value = volume.channel;
          // }
          // else {
          //   Channels.SetActive(false);
          // }
          sliderThresholdMax.value = volume.thresholdMax;
          sliderThresholdMin.value = volume.thresholdMin;
          sliderRotX.value = volume.rotx;
          sliderRotY.value = volume.roty;
          sliderRotZ.value = volume.rotz;
          sliderScale.value = volume.scale;
          sliderQuality.value = volume.Quality;
          sliderContrast.value = volume.contrast;
          sliderIntensity.value = volume.intensity;
          sliderTimepoint.value = volume.timepoint;
          sliderTimepoint.maxValue = volume.maxtimepoint;
          sliderRate.value = volume.rate;
          sliderColorR.value = volume.color.r;
          sliderColorG.value = volume.color.g;
          sliderColorB.value = volume.color.b;

        }

        // =========================
        // HERE ARE THE EVENTS CALLS
        // =========================

        public void ToggleSetValue(Toggle instance, bool value)
        {
          var originalEvent = instance.onValueChanged;
          instance.onValueChanged = emptyToggleEvent;
          instance.isOn = value;
          instance.onValueChanged = originalEvent;
        }

        public void SliderSetValue(Slider instance, float value)
        {
          var originalEvent = instance.onValueChanged;
          instance.onValueChanged = emptySliderEvent;
          instance.value = value;
          instance.onValueChanged = originalEvent;
        }

        public void ScrollbarSetValue(Scrollbar instance, float value)
        {
          var originalEvent = instance.onValueChanged;
          instance.onValueChanged = emptyScrollbarEvent;
          instance.value = value;
          instance.onValueChanged = originalEvent;
        }

        public void OnSliceXMinChange() {
          float size = sliderXMax.value - sliderXMin.value;
          float value = sliderXMin.value / (1 - size);
          SliderSetValue(sliderXSize, size);
          scrollbarXCenter.size = size;
          ScrollbarSetValue(scrollbarXCenter, value);
          volume.sliceXMin = sliderXMin.value = Mathf.Min(sliderXMin.value, volume.sliceXMax - sliceThreshold);

        }

        public void OnSliceXMaxChange() {
          float size = sliderXMax.value - sliderXMin.value;
          float value = sliderXMin.value / (1 - size);
          SliderSetValue(sliderXSize, size);
          scrollbarXCenter.size = size;
          ScrollbarSetValue(scrollbarXCenter, value);
          volume.sliceXMax = sliderXMax.value = Mathf.Max(sliderXMax.value, volume.sliceXMin + sliceThreshold);

        }

        public void OnSliceXCenterChange() {
          float xmin = (1-scrollbarXCenter.size) * scrollbarXCenter.value;
          float xmax = xmin +  scrollbarXCenter.size;
          SliderSetValue(sliderXMin, xmin);
          SliderSetValue(sliderXMax, xmax);
          volume.sliceXMax = sliderXMax.value;
          volume.sliceXMin = sliderXMin.value;

        }

        public void OnSliceXSizeChange() {
          scrollbarXCenter.size = sliderXSize.value;
          float xmin = (1-scrollbarXCenter.size) * scrollbarXCenter.value;
          float xmax = xmin +  scrollbarXCenter.size;
          SliderSetValue(sliderXMin, xmin);
          SliderSetValue(sliderXMax, xmax);
          volume.sliceXMax = sliderXMax.value;
          volume.sliceXMin = sliderXMin.value;

        }
        public void OnSliceYMinChange() {
          float size = sliderYMax.value - sliderYMin.value;
          float value = sliderYMin.value / (1 - size);
          SliderSetValue(sliderYSize, size);
          scrollbarYCenter.size = size;
          ScrollbarSetValue(scrollbarYCenter, value);
          volume.sliceYMin = sliderYMin.value = Mathf.Min(sliderYMin.value, volume.sliceYMax - sliceThreshold);

        }

        public void OnSliceYMaxChange() {
          float size = sliderYMax.value - sliderYMin.value;
          float value = sliderYMin.value / (1 - size);
          SliderSetValue(sliderYSize, size);
          scrollbarYCenter.size = size;
          ScrollbarSetValue(scrollbarYCenter, value);
          volume.sliceYMax = sliderYMax.value = Mathf.Max(sliderYMax.value, volume.sliceYMin + sliceThreshold);

        }

        public void OnSliceYCenterChange() {
          float xmin = (1-scrollbarYCenter.size) * scrollbarYCenter.value;
          float xmax = xmin +  scrollbarYCenter.size;
          SliderSetValue(sliderYMin, xmin);
          SliderSetValue(sliderYMax, xmax);
          volume.sliceYMax = sliderYMax.value;
          volume.sliceYMin = sliderYMin.value;

        }

        public void OnSliceYSizeChange() {
          scrollbarYCenter.size = sliderYSize.value;
          float xmin = (1-scrollbarYCenter.size) * scrollbarYCenter.value;
          float xmax = xmin +  scrollbarYCenter.size;
          SliderSetValue(sliderYMin, xmin);
          SliderSetValue(sliderYMax, xmax);
          volume.sliceYMax = sliderYMax.value;
          volume.sliceYMin = sliderYMin.value;

        }

        public void OnSliceZMinChange() {
          float size = sliderZMax.value - sliderZMin.value;
          float value = sliderZMin.value / (1 - size);
          SliderSetValue(sliderZSize, size);
          scrollbarZCenter.size = size;
          ScrollbarSetValue(scrollbarZCenter, value);
          volume.sliceZMin = sliderZMin.value = Mathf.Min(sliderZMin.value, volume.sliceZMax - sliceThreshold);

        }

        public void OnSliceZMaxChange() {
          float size = sliderZMax.value - sliderZMin.value;
          float value = sliderZMin.value / (1 - size);
          SliderSetValue(sliderZSize, size);
          scrollbarZCenter.size = size;
          ScrollbarSetValue(scrollbarZCenter, value);
          volume.sliceZMax = sliderZMax.value = Mathf.Max(sliderZMax.value, volume.sliceZMin + sliceThreshold);

        }

        public void OnSliceZCenterChange() {
          float xmin = (1-scrollbarZCenter.size) * scrollbarZCenter.value;
          float xmax = xmin +  scrollbarZCenter.size;
          SliderSetValue(sliderZMin, xmin);
          SliderSetValue(sliderZMax, xmax);
          volume.sliceZMax = sliderZMax.value;
          volume.sliceZMin = sliderZMin.value;

        }

        public void OnSliceZSizeChange() {
          scrollbarZCenter.size = sliderZSize.value;
          float xmin = (1-scrollbarZCenter.size) * scrollbarZCenter.value;
          float xmax = xmin +  scrollbarZCenter.size;
          SliderSetValue(sliderZMin, xmin);
          SliderSetValue(sliderZMax, xmax);
          volume.sliceZMax = sliderZMax.value;
          volume.sliceZMin = sliderZMin.value;

        }

        public void OnThresholdMinChange() {
          float size = sliderThresholdMax.value - sliderThresholdMin.value;
          float value = sliderThresholdMin.value / (1 - size);
          SliderSetValue(sliderThresholdSize, size);
          scrollbarThresholdCenter.size = size;
          ScrollbarSetValue(scrollbarThresholdCenter, value);
          volume.thresholdMin = sliderThresholdMin.value = Mathf.Min(sliderThresholdMin.value, volume.thresholdMax - sliceThreshold);
        }

        public void OnThresholdMaxChange() {
          float size = sliderThresholdMax.value - sliderThresholdMin.value;
          float value = sliderThresholdMin.value / (1 - size);
          SliderSetValue(sliderThresholdSize, size);
          scrollbarThresholdCenter.size = size;
          ScrollbarSetValue(scrollbarThresholdCenter, value);
          volume.thresholdMax = sliderThresholdMax.value = Mathf.Max(sliderThresholdMax.value, volume.thresholdMin + sliceThreshold);
        }

        public void OnThresholdCenterChange() {
          float xmin = (1-scrollbarThresholdCenter.size) * scrollbarThresholdCenter.value;
          float xmax = xmin +  scrollbarThresholdCenter.size;
          SliderSetValue(sliderThresholdMin, xmin);
          SliderSetValue(sliderThresholdMax, xmax);
          volume.thresholdMax = sliderThresholdMax.value;
          volume.thresholdMin = sliderThresholdMin.value;
        }

        public void OnThresholdSizeChange() {
          scrollbarThresholdCenter.size = sliderThresholdSize.value;
          float xmin = (1-scrollbarThresholdCenter.size) * scrollbarThresholdCenter.value;
          float xmax = xmin +  scrollbarThresholdCenter.size;
          SliderSetValue(sliderThresholdMin, xmin);
          SliderSetValue(sliderThresholdMax, xmax);
          volume.thresholdMax = sliderThresholdMax.value;
          volume.thresholdMin = sliderThresholdMin.value;
        }

        // public void OnChannelChanged() {
        //
        // volume.channel = channel;
        // volume.changeTexture();
        // }

        public void OnChannelChanged(int channel) {
          print("Channels Changed");
          print(channel);
          switch (channel) {
            case 1:
              volume.c1 = !volume.c1;
            break;
            case 2:
              volume.c2 = !volume.c2;
            break;
            case 3:
              volume.c3 = !volume.c3;
            break;
          }
          // switch (channel) {
          //   case 1:
          //     ToggleSetValue(Channel1, true);
          //     ToggleSetValue(Channel2, false);
          //     ToggleSetValue(Channel3, false);
          //   break;
          //   case 2:
          //     ToggleSetValue(Channel1, false);
          //     ToggleSetValue(Channel2, true);
          //     ToggleSetValue(Channel3, false);
          //   break;
          //   case 3:
          //     ToggleSetValue(Channel1, false);
          //     ToggleSetValue(Channel2, false);
          //     ToggleSetValue(Channel3, true);
          //   break;
          // }
            // if (channel <= volume.maxChannel) {
            //   volume.channel = channel - 1;
            //   volume.changeTexture();
            // }
        }

        public void OnRotationXChanged() {
          volume.rotx = sliderRotX.value;
          volume.Rotate();
        }

        public void OnRotationYChanged() {
          volume.roty = sliderRotY.value;
          volume.Rotate();
        }

        public void OnRotationZChanged() {
          volume.rotz = sliderRotZ.value;
          volume.Rotate();
        }

        public void OnScaleChanged() {
          volume.Scale(sliderScale.value);
        }

        public void OnQualityChanged() {
          volume.Quality = sliderQuality.value;
        }

        public void OnContrastChanged() {
          volume.contrast = sliderContrast.value;
        }

        public void OnColorRChange() {
          volume.color.r = sliderColorR.value;
          SetColor();
        }
        public void OnColorGChange() {
          volume.color.g = sliderColorG.value;
          SetColor();
        }
        public void OnColorBChange() {
          volume.color.b = sliderColorB.value;
          SetColor();
        }

        public void SetColor() {
          ImageColor.GetComponent<Image>().color = volume.color;
        }

        public void OnIntensityChanged() {
          print("intensity");
          print(sliderIntensity.value);

          volume.intensity = sliderIntensity.value;
        }

        public void OnTimepointChanged() {
          print(sliderTimepoint.value);
          volume.timepoint = (int)sliderTimepoint.value;
          sliderTimepoint.maxValue = volume.maxtimepoint;
          volume.changeTexture();
          TextTimepoint.text = string.Format("{0}", volume.timepoint);
        }

        public void OnRateChanged() {
          volume.rate = sliderRate.value;
        }

// =========================
// HERE ARE THE BUTTON CALLS
// =========================

        public void Play()
        {
          print("Play !");
          volume.play = !volume.play;
        }

        public void OnNextTimePoint()
        {
          if (volume.timepoint + 1 <= volume.maxtimepoint){
            volume.timepoint += 1;
            volume.changeTexture();
            sliderTimepoint.value += 1;
            TextTimepoint.text = string.Format("{0}", volume.timepoint);
          }
        }

        public void OnPreviousTimePoint()
        {
          if (volume.timepoint - 1 >= 0){
            volume.timepoint -= 1;
            volume.changeTexture();
            sliderTimepoint.value -= 1;
            TextTimepoint.text = string.Format("{0}", volume.timepoint);
          }
        }

        public void Quit()
        {
          VolumeRenderingNew volume = Container.GetComponentInChildren<VolumeRenderingNew>();
          volume.Database.RemoveContainer(volume.DatabaseVolumeID);
          Destroy(Container);
        }
    }

}
