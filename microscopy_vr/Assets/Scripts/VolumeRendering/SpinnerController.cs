﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

namespace VolumeRendering
{

    public class SpinnerController : MonoBehaviour {

        [SerializeField] public Slider LoadingBar;

        void Start ()
        {
          LoadingBar.value = 0f;
        }
    }

}
