﻿
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;
using UnityEngine.UI;

using VRTK;

namespace VolumeRendering
{

    public class VolumeCreator : MonoBehaviour {


      public VolumeDatabase Database;
      public GameObject ContainerPrefab;

      public GameObject button00;
      public GameObject button01;
      public GameObject button02;
      public GameObject button10;
      public GameObject button11;
      public GameObject button12;
      public GameObject button20;
      public GameObject button21;
      public GameObject button22;

      public GameObject Loading;


      public GameObject RightController;

      [SerializeField] protected GameObject Next;
      [SerializeField] protected GameObject Previous;

      private int Page = 0;
      private int LastPage = 0;

      public List<GameObject> Grid;

      private VRTK_DeviceFinder.Devices LeftController;

      public void Update() {
          if (Database.Loading) {
            Loading.SetActive(true);
            HideGrid();
          }
          else {
            Loading.SetActive(false);
            ShowGrid();
          }
        LastPage = Page;
      }

      public void Start() {
        // print(button02);
        // print(Next);
        // Next = GameObject.Find("VolumeCreator/Next") as Button;
        // print(Next);
        // Next.SetActive(false);
        // Grid = new List<Button>();
        // for (int i=0; i<9; i++) {
        //   Grid[i].SetActive(false);
        // }
        // LeftController = ;


        Next.SetActive(false);
        Previous.SetActive(false);

        StartCoroutine("Wait");

        // SetPrevNext();
      }

      IEnumerator Wait() {
        while (!Database.Ready) {
            yield return null;
          }
          Grid.Add(button00);
          Grid.Add(button01);
          Grid.Add(button02);
          Grid.Add(button10);
          Grid.Add(button11);
          Grid.Add(button12);
          Grid.Add(button20);
          Grid.Add(button21);
          Grid.Add(button22);
          FillPage();
          SetPrevNext();
      }


      public void next() {
        Page += 1;
        FillPage();
        SetPrevNext();
      }

      public void previous() {
        Page -= 1;
        FillPage();
        SetPrevNext();
      }

      public void SetPrevNext() {
        print(string.Format("DB COUNT {0}", Database.Datasets.Count));

        if ((Page * 9) + 9 < Database.Datasets.Count) {
          Next.SetActive(true);
        }
        else {
          Next.SetActive(false);
        }
        if (Page > 0) {
          Previous.SetActive(true);
        }
        else {
          Previous.SetActive(false);
        }
      }

      public void HideGrid() {
        for (int i = 0; i<9; i++ ) {
            Grid[i].SetActive(false);
        }
      }

      public void ShowGrid() {
        if (Grid.Count == 9) {
          for (int i = 0; i<9; i++ ) {
            int ID = (Page * 9) + i;
            if (ID < Database.Datasets.Count) {
              Grid[i].SetActive(true);
            }
          }
        }
      }

      public void FillPage() {
        for (int i = 0; i<9; i++ ) {
          int ID = (Page * 9) + i;
          if (ID < Database.Datasets.Count) {
            Grid[i].SetActive(true);
            Button button = Grid[i].GetComponent<Button>();
            Text b = Grid[i].GetComponentInChildren(typeof(Text)) as Text;
            b.text = Database.Datasets[ID];
            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(delegate { Create(ID); });
          }
          else {
            Grid[i].SetActive(false);
          }
        }
      }

      public void Create(int index) {
        if (!Database.Loading) {
          GameObject Container = Instantiate(ContainerPrefab) as GameObject;

          Transform Cube = Container.transform.GetChild(0);

          RightController.GetComponent<VRTK_ObjectAutoGrab>().objectToGrab = Cube.GetComponent<VRTK_InteractableObject>();
          RightController.GetComponent<VRTK_ObjectAutoGrab>().enabled = true;

          // Transform Cube = cubeContainer.transform.GetChild(0);

          int ObjIndex = Database.AddContainer(Container);
          Cube.GetComponent<VolumeRenderingNew>().DatasetID = index;
          Cube.GetComponent<VolumeRenderingNew>().DatabaseVolumeID = ObjIndex;
          Cube.GetComponent<VolumeRenderingNew>().Database = Database;
          if (!Database.Loaded[index]) {
            Database.LoadBackground(index);
          }
        }
      }

    }

}
