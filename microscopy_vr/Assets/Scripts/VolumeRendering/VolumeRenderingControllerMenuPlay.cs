  ﻿using System.Collections;
  using System.Collections.Generic;

  using UnityEngine;
  using UnityEngine.EventSystems;
  using UnityEngine.UI;

  namespace VolumeRendering
  {

      public class VolumeRenderingControllerMenuPlay : MonoBehaviour {

          public enum MenuStates {Play, Loading, NotSelected};
          public MenuStates currentState;

          public GameObject MenuPlay;
          public GameObject MenuLoading;
          public GameObject MenuNotSelected;

          public VolumeRenderingControllerNew Controller;

          private int lastIndex = -1;
          private bool Loaded = false;

        void Update () {
          if (!Controller.volume) {
            currentState = MenuStates.NotSelected;
          }
          else {
            if (Controller.currentVolumeIndex != lastIndex) {
              if (!Controller.volume.Loaded) {
                currentState = MenuStates.Loading;
                Loaded = false;
              }
              else {
                currentState = MenuStates.Play;
              }
            }
            else {
              if (!Controller.volume.Loaded) {
                currentState = MenuStates.Loading;
              }
              else {
                if (!Loaded) {
                  currentState = MenuStates.Play;
                  Loaded = true;
                }
              }
            }
          }
          MenuPlay.SetActive(false);
          MenuLoading.SetActive(false);
          MenuNotSelected.SetActive(false);
          switch (currentState) {
            case MenuStates.Play:
              MenuPlay.SetActive(true);
            break;
            case MenuStates.Loading:
              MenuLoading.SetActive(true);
            break;
            case MenuStates.NotSelected:
              MenuNotSelected.SetActive(true);
            break;
          }
          if (Controller) {
            lastIndex = Controller.currentVolumeIndex;
          }
        }

        void Start ()
        {
          currentState = MenuStates.NotSelected;
        }


        public void OnPlay() {
            currentState = MenuStates.Play;
        }


    }

}
