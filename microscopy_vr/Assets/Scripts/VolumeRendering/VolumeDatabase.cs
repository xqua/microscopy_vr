﻿
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Threading;

using UnityEngine;
// using UnityEditor;

namespace VolumeRendering
{

    public class VolumeDatabase : MonoBehaviour {

        public List<string> Datasets;
        public List<bool> Loaded;
        public List<Volume> Database;
        public List<Texture3D[,]> Datas;
        public List<VolumeSettings> Settings;
        public List<Thread> Threads;
        public List<GameObject> VolumeContainers;
        public VolumeRenderingControllerNew Controller;
        public int ActiveVolume;
        public bool Loading;
        public bool Ready = false;

        protected void Start () {
          this.Datas = new List<Texture3D[,]>();
          // Database = new List<Database>();
          // Settings  = new List<VolumeSettings>();
          Threads = new List<Thread>();
          string texturePath = Path.Combine(Application.dataPath, "Textures");
          if (!Directory.Exists(texturePath))
                   {
                       Directory.CreateDirectory(texturePath);
                   }
          print(texturePath);
          List<string> tmpdirs = new List<string>(Directory.GetDirectories(texturePath));
          foreach (var dir in tmpdirs)
          {
            print(Path.Combine(Path.GetFileName(dir), "settings.json"));
            if (File.Exists(Path.Combine(texturePath, Path.Combine(Path.GetFileName(dir), "settings.json"))))
            {
              print(Path.GetFileName(dir));
              Datasets.Add(Path.GetFileName(dir));
              Loaded.Add(false);
              VolumeSettings settings = loadTex3DSettings(Path.Combine(texturePath, Path.Combine(Path.GetFileName(dir), "settings.json")));
              Settings.Add(settings);
              Volume v = new Volume();
              v.SetPath(Path.Combine(texturePath, Path.GetFileName(dir)));
              Database.Add(v);
              Thread thread = new Thread(new ParameterizedThreadStart(Load));
              Threads.Add(thread);
              Texture3D[,] t3D = new Texture3D[settings.channels, settings.timepoints];
              Datas.Add(t3D);
            }
          }
          Ready = true;
        }

        public static Texture3D CreateTexture3D(int c, int t, int index, VolumeSettings settings, List<Volume> Database)
        {
            int width = settings.x;
            int height = settings.y;
            int depth = settings.z;
            TextureFormat format;
            // var tex  = new Texture3D(width, height, depth, TextureFormat.ARGB32, false);
            if (settings.channels > 1) {
              format = TextureFormat.RGBA32;
            }
            else {
              format = TextureFormat.Alpha8;
            }
            var tex  = new Texture3D(width, height, depth, format, false);
            tex.wrapMode = TextureWrapMode.Clamp;
            tex.filterMode = FilterMode.Trilinear;
            tex.anisoLevel = 0;
            // tex.LoadRawTextureData(Database[index].Data);
            Debug.Log("Creating Texture");
            Debug.Log(format);
            Color[] colors = new Color[Database[index].Volumes[0,t].Length];
            for (int i = 0; i < settings.channels; i++) {
              for (int j = 0; j < Database[index].Volumes[i,t].Length; j++) {
                Debug.Log("Doing Channel");
                Debug.Log(i);
                if (settings.channels == 1) {
                  colors[j].a += Database[index].Volumes[i,t][j].a;
                }
                else {
                  colors[j] += Database[index].Volumes[i,t][j];
                  colors[j].a = 1;
                  // if (colors[j].r > 0.1) {
                  //   Debug.Log(colors[j]);
                  // }
                  // if (colors[j].g > 0.1) {
                  //   Debug.Log(colors[j]);
                  // }
                }
              }
            }
            tex.SetPixels(colors);
            tex.Apply();
            return tex;
        }

        IEnumerator Wait(int index) {
          // Loading the TimeLapse into RAM for fluidity!
          Threads[index].Start(index);
          while (!Database[index].Done) {
              // Spinner.LoadingBar.value = Database.Database[DatasetID].Progress;
              yield return null;
            }
            print("LoadingTextures");
            makeTex3D(index);
            Loading = false;
            yield return 0;
        }

        public void makeTex3D(int index) {
          for (int c = 0; c < Settings[index].channels; c++) {
            for (int t = 0; t < Settings[index].timepoints; t++) {
              Datas[index][c,t] = CreateTexture3D(c, t, index, Settings[index], Database);
            }
          }
          Loaded[index] = true;
        }

        public void Load(object obj) {
            int index = (int)obj;
            print("Load DB start");
            Database[index].Load();

        }

        public void LoadBackground(int index) {
          if (!Loaded[index]){
              if (!Loading) {
                StartCoroutine(Wait(index));
                Loading = true;
              }
              else {
                print("Already Loading!");
              }
          }
        }

        private void OnDestroy()
        {
          for (int i =0; i < Threads.Count; i++) {
            Threads[i].Abort();
          }
        }

        private void OnApplicationQuit()
        {
          for (int i =0; i < Threads.Count; i++) {
            Threads[i].Abort();
          }
        }

        public int AddContainer(GameObject container) {
          VolumeContainers.Add(container);
          int index =  VolumeContainers.Count - 1;
          return index;
        }

        public void RemoveContainer(int index) {
          VolumeContainers.RemoveAt(index);
        }

        public void SetActive(int index) {
          foreach (var container in VolumeContainers) {
            container.GetComponentInChildren<VolumeRenderingNew>().SetSelected(false);
          }
          ActiveVolume = index;
          VolumeContainers[index].GetComponentInChildren<VolumeRenderingNew>().SetSelected(true);
          Controller.SetVolume(index);
        }

        private static VolumeSettings loadTex3DSettings(string filePath)
        {
          // string filePath = Path.Combine(Application.dataPath, Path.Combine(path , "settings.json"));
          // Read the json from the file into a string
          string dataAsJson = File.ReadAllText(filePath);
          // Pass the json to JsonUtility, and tell it to create a GameData object from it
          VolumeSettings settings = VolumeSettings.CreateFromJSON(dataAsJson);
          return settings;

        }

    }

}
