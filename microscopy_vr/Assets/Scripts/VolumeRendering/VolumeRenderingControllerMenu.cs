  ﻿using System.Collections;
  using System.Collections.Generic;

  using UnityEngine;
  using UnityEngine.EventSystems;
  using UnityEngine.UI;

  namespace VolumeRendering
  {

      public class VolumeRenderingControllerMenu : MonoBehaviour {

          public enum MenuStates {Main, Slice, SliceX_1, SliceY_1, SliceZ_1, SliceX_2, SliceY_2, SliceZ_2, Intensity, Quality, Threshold, Threshold_1, Threshold_2, Rotation, Loading, NotSelected};
          public MenuStates currentState;

          public GameObject MenuMain;
          public GameObject MenuSlice;
          public GameObject MenuSliceX_1;
          public GameObject MenuSliceY_1;
          public GameObject MenuSliceZ_1;
          public GameObject MenuSliceX_2;
          public GameObject MenuSliceY_2;
          public GameObject MenuSliceZ_2;
          public GameObject MenuIntensity;
          public GameObject MenuQuality;
          public GameObject MenuThreshold;
          public GameObject MenuThreshold_1;
          public GameObject MenuThreshold_2;
          public GameObject MenuRotation;
          public GameObject MenuLoading;
          public GameObject MenuNotSelected;

          public VolumeRenderingControllerNew Controller;

          private int lastIndex = -1;
          private bool Loaded = false;

        void Update () {
          if (!Controller.volume) {
            currentState = MenuStates.NotSelected;
          }
          else {
            if (Controller.currentVolumeIndex != lastIndex) {
              if (!Controller.volume.Loaded) {
                currentState = MenuStates.Loading;
                Loaded = false;
              }
              else {
                currentState = MenuStates.Main;
              }
            }
            else {
              if (!Controller.volume.Loaded) {
                currentState = MenuStates.Loading;
              }
              else {
                if (!Loaded) {
                  currentState = MenuStates.Main;
                  Loaded = true;
                }
              }
            }
          }
          MenuMain.SetActive(false);
          MenuSlice.SetActive(false);
          MenuSliceX_1.SetActive(false);
          MenuSliceY_1.SetActive(false);
          MenuSliceZ_1.SetActive(false);
          MenuSliceX_2.SetActive(false);
          MenuSliceY_2.SetActive(false);
          MenuSliceZ_2.SetActive(false);
          MenuIntensity.SetActive(false);
          MenuQuality.SetActive(false);
          MenuThreshold.SetActive(false);
          MenuThreshold_1.SetActive(false);
          MenuThreshold_2.SetActive(false);
          MenuRotation.SetActive(false);
          MenuLoading.SetActive(false);
          MenuNotSelected.SetActive(false);
          switch (currentState) {
            case MenuStates.Main:
              MenuMain.SetActive(true);
            break;
            case MenuStates.Slice:
              MenuSlice.SetActive(true);
            break;
            case MenuStates.SliceX_1:
              MenuSliceX_1.SetActive(true);
            break;
            case MenuStates.SliceY_1:
              MenuSliceY_1.SetActive(true);
            break;
            case MenuStates.SliceZ_1:
              MenuSliceZ_1.SetActive(true);
            break;
            case MenuStates.SliceX_2:
              MenuSliceX_2.SetActive(true);
            break;
            case MenuStates.SliceY_2:
              MenuSliceY_2.SetActive(true);
            break;
            case MenuStates.SliceZ_2:
              MenuSliceZ_2.SetActive(true);
            break;
            case MenuStates.Intensity:
              MenuIntensity.SetActive(true);
            break;
            case MenuStates.Quality:
              MenuQuality.SetActive(true);
            break;
            case MenuStates.Threshold:
              MenuThreshold.SetActive(true);
            break;
            case MenuStates.Threshold_1:
              MenuThreshold_1.SetActive(true);
            break;
            case MenuStates.Threshold_2:
              MenuThreshold_2.SetActive(true);
            break;
            case MenuStates.Rotation:
              MenuRotation.SetActive(true);
            break;
            case MenuStates.Loading:
              MenuLoading.SetActive(true);
            break;
            case MenuStates.NotSelected:
              MenuNotSelected.SetActive(true);
            break;
          }
          if (Controller) {
            lastIndex = Controller.currentVolumeIndex;
          }
        }

        void Start ()
        {
          currentState = MenuStates.NotSelected;
        }


        public void OnMain() {
            currentState = MenuStates.Main;
        }

        public void OnSlice() {
            currentState = MenuStates.Slice;
        }

        public void OnSliceX_1() {
            currentState = MenuStates.SliceX_1;
        }

        public void OnSliceY_1() {
            currentState = MenuStates.SliceY_1;
        }

        public void OnSliceZ_1() {
            currentState = MenuStates.SliceZ_1;
        }

        public void OnSliceX_2() {
            currentState = MenuStates.SliceX_2;
        }

        public void OnSliceY_2() {
            currentState = MenuStates.SliceY_2;
        }

        public void OnSliceZ_2() {
            currentState = MenuStates.SliceZ_2;
        }

        public void OnIntensity() {
            currentState = MenuStates.Intensity;
        }

        public void OnQuality() {
            currentState = MenuStates.Quality;
        }

        public void OnThreshold() {
            currentState = MenuStates.Threshold;
        }

        public void OnThreshold_1() {
            currentState = MenuStates.Threshold_1;
        }

        public void OnThreshold_2() {
            currentState = MenuStates.Threshold_2;
        }

        public void OnRotation() {
            currentState = MenuStates.Rotation;
        }

    }

}
