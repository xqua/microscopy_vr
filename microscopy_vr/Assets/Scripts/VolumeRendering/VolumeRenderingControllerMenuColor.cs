  ﻿using System.Collections;
  using System.Collections.Generic;

  using UnityEngine;
  using UnityEngine.EventSystems;
  using UnityEngine.UI;

  namespace VolumeRendering
  {

      public class VolumeRenderingControllerMenuColor : MonoBehaviour {

          public enum MenuStates {Color, Loading, NotSelected};
          public MenuStates currentState;

          public GameObject MenuColor;
          public GameObject MenuLoading;
          public GameObject MenuNotSelected;

          public VolumeRenderingControllerNew Controller;

          private int lastIndex = -1;
          private bool Loaded = false;

        void Update () {
          if (!Controller.volume) {
            currentState = MenuStates.NotSelected;
          }
          else {
            if (Controller.currentVolumeIndex != lastIndex) {
              if (!Controller.volume.Loaded) {
                currentState = MenuStates.Loading;
                Loaded = false;
              }
              else {
                currentState = MenuStates.Color;
              }
            }
            else {
              if (!Controller.volume.Loaded) {
                currentState = MenuStates.Loading;
              }
              else {
                if (!Loaded) {
                  currentState = MenuStates.Color;
                  Loaded = true;
                }
              }
            }
          }
          MenuColor.SetActive(false);
          MenuLoading.SetActive(false);
          MenuNotSelected.SetActive(false);
          switch (currentState) {
            case MenuStates.Color:
              MenuColor.SetActive(true);
            break;
            case MenuStates.Loading:
              MenuLoading.SetActive(true);
            break;
            case MenuStates.NotSelected:
              MenuNotSelected.SetActive(true);
            break;
          }
          if (Controller) {
            lastIndex = Controller.currentVolumeIndex;
          }
        }

        void Start ()
        {
          currentState = MenuStates.NotSelected;
        }


        public void OnColor() {
            currentState = MenuStates.Color;
        }


    }

}
