﻿
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;
//using UnityEditor;

namespace VolumeRendering
{

    public class Volume : MonoBehaviour {

        public float Progress = 0.0f;
        private float Step;
        public bool Done = false;

        private string DataPath;

        private static VolumeSettings settings;
        public Color[,][] Volumes;


        public void SetPath(string path)
        {
          DataPath = path;
        }

        public void Load() {
          // Loading the TimeLapse into RAM for fluidity!
          DateTime t0;
          settings = loadTex3DSettings(DataPath);
          Step = 10000.0f / (settings.channels * (settings.timepoints) * settings.x * settings.y * settings.z);
          Volumes = new Color[settings.channels, settings.timepoints][];
          lock (DataPath) {
            for (int c = 0; c < settings.channels; c++) {
              for (int t = 0; t < settings.timepoints; t++) {
                string tex = String.Format("c{0}_t{1:D5}.raw", c, t);
                string path = Path.Combine(DataPath , tex);
                t0 = DateTime.Now;
                Volumes[c,t] = this.LoadRAWfast(path, c, settings.channels);
                Debug.LogWarning("Time Elapsed (old)");
                Debug.LogWarning(DateTime.Now - t0);
                // t0 = Time.time;
                // Volumes[c,t] = this.LoadRAW(path);
                // Debug.LogWarning("Time Elapsed (new)");
                // Debug.LogWarning(Time.time - t0);
              }
            }
          }
          print("Done Loading");
          Done = true;
        }

        private static VolumeSettings loadTex3DSettings(string path)
        {
          string filePath = Path.Combine(path , "settings.json");
          // Read the json from the file into a string
          string dataAsJson = File.ReadAllText(filePath);
          // Pass the json to JsonUtility, and tell it to create a GameData object from it
          VolumeSettings settings = VolumeSettings.CreateFromJSON(dataAsJson);
          return settings;

        }

        private Color[] LoadRAW(string path)
        {
          int width = settings.x;
          int height = settings.y;
          int depth = settings.z;

          var max = width * height * depth;

            using(var stream = new FileStream(path, FileMode.Open))
            {
                var len = stream.Length;
                if(len != max)
                {
                    Debug.LogWarning(path + " doesn't have required resolution");
                }

                int i = 0;
                Color[] colors = new Color[max];
                // Color[] colors = new Color[max];
                float inv = 1f / 255.0f;
                for(i = 0; i < stream.Length; i++)
                {
                    if (i % 10000 == 0) {
                      Progress += Step;
                    }
                    if(i == max)
                    {
                        break;
                    }
                    int v = stream.ReadByte();
                    float f = v * inv;
                    colors[i] = new Color(f, f, f, f);
                }
                return colors;
            }
        }

        private Color[] LoadRAWfast(string path, int c, int maxC)
        {
          int width = settings.x;
          int height = settings.y;
          int depth = settings.z;

          var size = width * height * depth;
          var stream = new FileStream(path, FileMode.Open);

          if(stream.Length != size)
          {
              Debug.LogWarning(path + " doesn't have required resolution");
          }

          float inv = 1f / 255.0f;

          byte[] buffer = new byte[size];
          int bytesRead = stream.Read(buffer, 0, buffer.Length);
          Color[] colors = new Color[size];
          for(int i = 0; i < size; i++)
          {
            if (i % 10000 == 0) {
              Progress += Step;
            }
            float f = buffer[i] * inv;
            if (maxC == 1) {
              colors[i] = new Color(f, f, f, f);
            }
            else {
              if (c == 0) {
                colors[i] = new Color(f, 0, 0, 0);
              }
              if (c == 1) {
                colors[i] = new Color(0, f, 0, 0);
              }
              if (c == 2) {
                colors[i] = new Color(0, 0, f, 0);
              }
            }
          }
          return colors;
        }

    }

}
