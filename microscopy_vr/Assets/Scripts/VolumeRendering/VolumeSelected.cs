using UnityEngine;
using System.Collections;


namespace VolumeRendering
{

public class VolumeSelected : MonoBehaviour {

  public GameObject Cube;
  public Vector3 StartPosition;
  public bool up = true;
  public float step = 0.01f;
  public float range = 0.1f;
  private bool move = true;

  void Start () {
    StartPosition = transform.position;
    StartCoroutine("Move");
  }

  public void Update() {
    Vector3 cubesize = Cube.GetComponent<Collider>().bounds.size;
    Vector3 cubecenter = Cube.transform.position;
    StartPosition.x = cubecenter.x;
    StartPosition.z = cubecenter.z;
    StartPosition.y = transform.position.y;
    transform.position = StartPosition;
    StartPosition.y = cubecenter.y + cubesize.y + 0.5f;

  }

	IEnumerator Move() {
    while (move) {
      Vector3 pos = transform.position;
      if (up) {
        pos.y += step;
        transform.position = pos;
        if (pos.y > StartPosition.y + range) {
          up = false;
        }
      }
      else {
        pos.y -= step;
        transform.position = pos;
        if (pos.y < StartPosition.y - range) {
          up = true;
        }
      }
      yield return null;
    }
	}

  void OnDisable()
  {
      move = false;
  }

  void OnEnable()
  {
      move = true;
      StartCoroutine("Move");
  }

}
}
