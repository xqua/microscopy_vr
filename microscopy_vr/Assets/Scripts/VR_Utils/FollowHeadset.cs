using UnityEngine;
using System.Collections;
using System;
using VRTK;

public class FollowHeadset : MonoBehaviour {
    // public Transform target;
    public Transform reference;
    public bool FollowY = false;

    // public static Transform HeadsetTransform();


    void Update() {
      // Look at including x and z leaning
      Transform target = VRTK_DeviceFinder.HeadsetTransform();
      transform.LookAt(target);

      // Euler angles are easier to deal with. You could use Quaternions here also
      // C# requires you to set the entire rotation variable. You can't set the individual x and z (UnityScript can), so you make a temp Vec3 and set it back
      Vector3 eulerAngles = transform.rotation.eulerAngles;
      eulerAngles.y = eulerAngles.y+180;
      eulerAngles.x = 0;
      eulerAngles.z = 0;
      Vector3 newpos = reference.position;

      if (FollowY) {
        newpos[1] = newpos[1];
      }
      else {
        newpos[1] = transform.position[1];
      }
      newpos[1] = Math.Max(1,newpos[1]);
      transform.position = newpos;
      // Set the altered rotation back
      transform.rotation = Quaternion.Euler(eulerAngles);
    }
}
