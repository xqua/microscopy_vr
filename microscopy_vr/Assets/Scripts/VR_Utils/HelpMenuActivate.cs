namespace VRTK.Examples
{
    using UnityEngine;

    public class HelpMenuActivate : MonoBehaviour
    {

        public GameObject HelpMenu;
        private bool HelpMenuActivated;

        private void Start()
        {
            if (GetComponent<VRTK_ControllerEvents>() == null)
            {
                VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_FROM_GAMEOBJECT, "VRTK_ControllerEvents_ListenerExample", "VRTK_ControllerEvents", "the same"));
                return;
            }

            //Setup controller event listeners

            GetComponent<VRTK_ControllerEvents>().ButtonTwoPressed += new ControllerInteractionEventHandler(DoButtonTwoPressed);
            // GetComponent<VRTK_ControllerEvents>().ButtonTwoReleased += new ControllerInteractionEventHandler(DoButtonTwoReleased);

            // GetComponent<VRTK_ControllerEvents>().ControllerEnabled += new ControllerInteractionEventHandler(DoControllerEnabled);
            // GetComponent<VRTK_ControllerEvents>().ControllerDisabled += new ControllerInteractionEventHandler(DoControllerDisabled);
            //
            // GetComponent<VRTK_ControllerEvents>().ControllerIndexChanged += new ControllerInteractionEventHandler(DoControllerIndexChanged);
        }

        private void DebugLogger(uint index, string button, string action, ControllerInteractionEventArgs e)
        {
            VRTK_Logger.Info("Controller on index '" + index + "' " + button + " has been " + action
                    + " with a pressure of " + e.buttonPressure + " / trackpad axis at: " + e.touchpadAxis + " (" + e.touchpadAngle + " degrees)");
        }

        private void DoButtonTwoPressed(object sender, ControllerInteractionEventArgs e)
        {
          if (HelpMenuActivated) {
            HelpMenu.SetActive(false);
            HelpMenuActivated = false;
          }
          else {
            HelpMenu.SetActive(true);
            Transform target = VRTK_DeviceFinder.HeadsetTransform();
            HelpMenu.transform.position = target.position;
            Quaternion rot = target.rotation;
            rot.x = 0;
            rot.z = 0;
            HelpMenu.transform.rotation = rot;
            HelpMenuActivated = true;
          }
            DebugLogger(VRTK_ControllerReference.GetRealIndex(e.controllerReference), "BUTTON TWO", "pressed down", e);
        }

        // private void DoButtonTwoReleased(object sender, ControllerInteractionEventArgs e)
        // {
        //     DebugLogger(VRTK_ControllerReference.GetRealIndex(e.controllerReference), "BUTTON TWO", "released", e);
        // }

        // private void DoControllerEnabled(object sender, ControllerInteractionEventArgs e)
        // {
        //     DebugLogger(VRTK_ControllerReference.GetRealIndex(e.controllerReference), "CONTROLLER STATE", "ENABLED", e);
        // }
        //
        // private void DoControllerDisabled(object sender, ControllerInteractionEventArgs e)
        // {
        //     DebugLogger(VRTK_ControllerReference.GetRealIndex(e.controllerReference), "CONTROLLER STATE", "DISABLED", e);
        // }
        //
        // private void DoControllerIndexChanged(object sender, ControllerInteractionEventArgs e)
        // {
        //     DebugLogger(VRTK_ControllerReference.GetRealIndex(e.controllerReference), "CONTROLLER STATE", "INDEX CHANGED", e);
        // }
    }
}
