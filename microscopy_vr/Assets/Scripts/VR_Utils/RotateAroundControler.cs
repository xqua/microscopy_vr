﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VRTK;


public class RotateAroundControler : MonoBehaviour {


    public VRTK_ControllerEvents events;
    protected float currentAngle; //Keep track of angle for when we click
    protected float lastAngle; //Keep track of angle for when we click
    protected float touchAngle; //Keep track of angle for when we click
    protected bool touchpadTouched;

    // Use this for initialization
    void Start () {

    }

	// Update is called once per frame
	void Update () {
        GameObject t = VRTK_DeviceFinder.GetControllerLeftHand();
        // if (t) {
        //events = t.GetComponentInParent<VRTK_ControllerEvents>();
        Quaternion newRot = t.transform.rotation;
        newRot *= Quaternion.Euler(0, 0, currentAngle);
        transform.rotation = newRot;
        transform.position = t.transform.position;
      // }

    }

    public void Teleport()
    {
        Transform playArea = VRTK_DeviceFinder.PlayAreaTransform();
        //newpos = playArea.position;
        //newpos.x = 0;
        //newpos.y = 0;
        //newpos.z = -36.37;
        playArea.position = new Vector3(0f,0f,-36.37f);
    }

    protected virtual void Initialize()
    {
        if (events == null)
        {
            events = GetComponentInParent<VRTK_ControllerEvents>();
        }
    }

    protected virtual float CalculateAngle(ControllerInteractionEventArgs e)
    {
        return 360 - e.touchpadAngle;
    }

    protected virtual void DoTouchpadTouched(object sender, ControllerInteractionEventArgs e)
    {
        touchpadTouched = true;
        touchAngle = CalculateAngle(e);
        lastAngle = currentAngle;
        //DoShowMenu(CalculateAngle(e));
    }

    protected virtual void DoTouchpadUntouched(object sender, ControllerInteractionEventArgs e)
    {
        touchpadTouched = false;

        //DoHideMenu(false);
    }

    //Touchpad finger moved position
    protected virtual void DoTouchpadAxisChanged(object sender, ControllerInteractionEventArgs e)
    {
        if (touchpadTouched)
        {
            DoChangeAngle(CalculateAngle(e));
        }
    }

    protected virtual void DoChangeAngle(float angle, object sender = null)
    {
        currentAngle = lastAngle + (angle - touchAngle);
        // print(currentAngle);
        //transform.Rotate(Vector3.forward * angle );
        //Quaternion originalRot = transform.rotation;
        //transform.rotation = originalRot * Quaternion.AngleAxis(angle, Vector3.forward);
        //transform.rotation = Quaternion.Euler(0, 0, angle);
        //menu.HoverButton(currentAngle);
    }

    protected virtual void OnEnable()
    {
        if (events == null)
        {
            VRTK_Logger.Error(VRTK_Logger.GetCommonMessage(VRTK_Logger.CommonMessageKeys.REQUIRED_COMPONENT_MISSING_NOT_INJECTED, "RadialMenuController", "VRTK_ControllerEvents", "events", "the parent"));
            return;
        }
        else
        {
            //events.TouchpadPressed += new ControllerInteractionEventHandler(DoTouchpadClicked);
            //events.TouchpadReleased += new ControllerInteractionEventHandler(DoTouchpadUnclicked);
            events.TouchpadTouchStart += new ControllerInteractionEventHandler(DoTouchpadTouched);
            events.TouchpadTouchEnd += new ControllerInteractionEventHandler(DoTouchpadUntouched);
            events.TouchpadAxisChanged += new ControllerInteractionEventHandler(DoTouchpadAxisChanged);

            //menu.FireHapticPulse += new HapticPulseEventHandler(AttemptHapticPulse);
        }
    }

    protected virtual void OnDisable()
    {
        //events.TouchpadPressed -= new ControllerInteractionEventHandler(DoTouchpadClicked);
        //events.TouchpadReleased -= new ControllerInteractionEventHandler(DoTouchpadUnclicked);
        events.TouchpadTouchStart -= new ControllerInteractionEventHandler(DoTouchpadTouched);
        events.TouchpadTouchEnd -= new ControllerInteractionEventHandler(DoTouchpadUntouched);
        events.TouchpadAxisChanged -= new ControllerInteractionEventHandler(DoTouchpadAxisChanged);

        //menu.FireHapticPulse -= new HapticPulseEventHandler(AttemptHapticPulse);
    }

}
