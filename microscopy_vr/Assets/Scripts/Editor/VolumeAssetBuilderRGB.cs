﻿using System.IO;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;

namespace VolumeRendering
{

    public class VolumeAssetBuilderRGB : EditorWindow {

        [MenuItem("Window/VolumeAssetBuilderRGB")]
        static void Init()
        {
            var window = EditorWindow.GetWindow(typeof(VolumeAssetBuilderRGB));
            window.Show();
        }

        string inputPathR, inputPathG, inputPathB, outputPath;
        int width = 256, height = 256, depth = 256;
        Object assetR;
        Object assetG;
        Object assetB;

        void OnEnable()
        {
            inputPathR = "Assets/MRI.256x256x256.raw";
            inputPathG = "Assets/MRI.256x256x256.raw";
            inputPathB = "Assets/MRI.256x256x256.raw";
            outputPath = "Assets/MRI.asset";
        }

        void OnGUI()
        {
            const float headerSize = 120f;

            using(new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("R: Input pvm raw file path", GUILayout.Width(headerSize));
                assetR = EditorGUILayout.ObjectField(assetR, typeof(Object), true);
                inputPathR = AssetDatabase.GetAssetPath(assetR);
            }
            using(new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("G: Input pvm raw file path", GUILayout.Width(headerSize));
                assetG = EditorGUILayout.ObjectField(assetG, typeof(Object), true);
                inputPathG = AssetDatabase.GetAssetPath(assetG);
            }
            using(new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("B: Input pvm raw file path", GUILayout.Width(headerSize));
                assetB = EditorGUILayout.ObjectField(assetB, typeof(Object), true);
                inputPathB = AssetDatabase.GetAssetPath(assetB);
            }

            using(new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Width", GUILayout.Width(headerSize));
                width = EditorGUILayout.IntField(width);
            }

            using(new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Height", GUILayout.Width(headerSize));
                height = EditorGUILayout.IntField(height);
            }

            using(new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Depth", GUILayout.Width(headerSize));
                depth = EditorGUILayout.IntField(depth);
            }

            using(new EditorGUILayout.HorizontalScope())
            {
                GUILayout.Label("Output path", GUILayout.Width(headerSize));
                outputPath = EditorGUILayout.TextField(outputPath);
            }

            if(GUILayout.Button("Build"))
            {
                Build(inputPathR, inputPathG, inputPathB, outputPath, width, height, depth);
            }
        }

        void Build(
            string inputPathR,
            string inputPathG,
            string inputPathB,
            string outputPath,
            int width,
            int height,
            int depth
        )
        {
            if (!File.Exists(inputPathR))
            {
                Debug.LogWarning(inputPathR + " is not exist.");
                return;
            }
            if (!File.Exists(inputPathG))
            {
                Debug.LogWarning(inputPathG + " is not exist.");
                return;
            }
            if (!File.Exists(inputPathB))
            {
                Debug.LogWarning(inputPathB + " is not exist.");
                return;
            }

            var volume = Build(inputPathR, inputPathG, inputPathB, width, height, depth);
            AssetDatabase.CreateAsset(volume, outputPath);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public static Texture3D Build(string pathR, string pathG, string pathB, int width, int height, int depth)
        {
            var size = width * height * depth;
            var tex  = new Texture3D(width, height, depth, TextureFormat.RGBA32, false);
            tex.wrapMode = TextureWrapMode.Clamp;
            tex.filterMode = FilterMode.Bilinear;
            tex.anisoLevel = 0;
            Color[] colors = new Color[size];
            float inv = 1f / 255.0f;
            int i = 0;

            using(var stream = new FileStream(pathR, FileMode.Open))
            {
                var len = stream.Length;
                if(len != size)
                {
                    Debug.LogWarning(pathR + " doesn't have required resolution");
                }

                byte[] buffer = new byte[size];
                int bytesRead = stream.Read(buffer, 0, buffer.Length);

                for(i = 0; i < size; i++)
                {
                    float f = buffer[i] * inv;
                    colors[i].r = f;
                }
            }
            using(var stream = new FileStream(pathG, FileMode.Open))
            {
                var len = stream.Length;
                if(len != size)
                {
                    Debug.LogWarning(pathG + " doesn't have required resolution");
                }

                byte[] buffer = new byte[size];
                int bytesRead = stream.Read(buffer, 0, buffer.Length);

                for(i = 0; i < size; i++)
                {
                    float f = buffer[i] * inv;
                    colors[i].g = f;
                }
            }
            using(var stream = new FileStream(pathB, FileMode.Open))
            {
                var len = stream.Length;
                if(len != size)
                {
                    Debug.LogWarning(pathB + " doesn't have required resolution");
                }

                byte[] buffer = new byte[size];
                int bytesRead = stream.Read(buffer, 0, buffer.Length);

                for(i = 0; i < size; i++)
                {
                    float f = buffer[i] * inv;
                    colors[i].b = f;
                    colors[i].a = 1;
                }
            }
            tex.SetPixels(colors);
            tex.Apply();
            return tex;
        }

    }

}
