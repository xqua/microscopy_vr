﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MamutModel;
using System;

namespace MamutRenderer {

	public class ModelControler : MonoBehaviour {

		public enum EdgeRenderTypes {LocalCenter, LocalBackward, LocalForward, All}
		public EdgeRenderTypes EdgeRenderType = EdgeRenderTypes.LocalCenter;
		public EdgeRenderTypes LastEdgeRenderType = EdgeRenderTypes.LocalCenter;
		public int Frame = 0;
		public int LastFrame = -1;
		public int LastTrackDepth = 10;
		public int TrackDepth = 10;
		public Model Model;

		public Slider FrameSlider;

		// // Use this for initialization
		void Start () {
			FrameSlider.onValueChanged.AddListener(delegate {OnFrameChange(); });
		}
		//
		// // Update is called once per frame
		void Update () {
			// CHANGED FOR FPS REASONS ! MAX Frame 100 !
			// Need to make the renderer a particle system ...
			FrameSlider.maxValue = Math.Min(Model.EdgeFrames.Count,100);
			// FrameSlider.maxValue = Model.EdgeFrames.Count;

		}

		public void OnDepthChange(float v) {
			TrackDepth = (int)v;
		}

		public void OnFrameChange() {
			Frame = (int)FrameSlider.value;
		}

		public void OnEdgeTypeChange() {

		}

	}
}
