﻿
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Xml.Linq;
using MamutModel;




using UnityEngine;
using UnityEngine.UI;

using VRTK;

namespace VolumeRendering
{

    public class DemoRetreat : MonoBehaviour {


      public VolumeDatabase Database;
      public VolumeCreator Creator;
      public GameObject ContainerPrefab;
      public Model Model;

      public void Update() {

      }

      public void Start() {

        StartCoroutine("LoadAll");
        // testLoadSmall();
        testLoadFull();
      }

      IEnumerator LoadAll() {
        while (!Database.Ready) {
            yield return null;
          }
        for (int i = 0; i < Database.Datasets.Count; i++) {
          while (Database.Loading) {
            yield return null;
          }
          Database.LoadBackground(i);
        }
        CreateInPlace();
      }


      public void CreateInPlace() {
        int radius = 5;
        for (int index = 0; index < Database.Datasets.Count; index++) {
          double angle = index * (360.0/Database.Datasets.Count);

          GameObject Container = Instantiate(ContainerPrefab) as GameObject;

          Transform Cube = Container.transform.GetChild(0);

          float x = radius * (float)Math.Cos(angle);
          float z = radius * (float)Math.Sin(angle);

          Cube.position = new Vector3(x, 1.5f, z);

          int ObjIndex = Database.AddContainer(Container);
          Cube.GetComponent<VolumeRenderingNew>().DatasetID = index;
          Cube.GetComponent<VolumeRenderingNew>().DatabaseVolumeID = ObjIndex;
          Cube.GetComponent<VolumeRenderingNew>().Database = Database;
        }
      }

      public void DestroyAll() {
        for (int index = 0; index < Database.VolumeContainers.Count; index++) {
          GameObject Container = Database.VolumeContainers[index];
          Destroy(Container);
        }
        Database.VolumeContainers.Clear();
      }

      public void Reset() {
        DestroyAll();
        CreateInPlace();
      }


  		public void testLoadFull() {
        string path = Path.Combine(Application.dataPath, "MamutXML/Bro1-fused2-mamut.xml");
  			// path = "/home/lblondel/Documents/Harvard/ExtavourLab/projects/Project_Parhyale/Microscopy/JaneliaLightSheet/Bro1/Mamut/UnityTest.xml";
  			// G = new Graph();
  			StartLoading(path);
  		}

      public void testLoadSmall() {
        string path = Path.Combine(Application.dataPath, "MamutXML/UnityTest.xml");
        // path = "/home/lblondel/Documents/Harvard/ExtavourLab/projects/Project_Parhyale/Microscopy/JaneliaLightSheet/Bro1/Mamut/UnityTest.xml";
        // G = new Graph();
        StartLoading(path);
      }

  		public void StartLoading(string path) {
  			StartCoroutine(Load(path));
  		}

  		public IEnumerator Load(string path) {
  			int spots = 0;
  			int edge = 0;
  			int nspots = 0;
  			XDocument xdoc = XDocument.Load(path);
  			List<string> TrackEdgeList = new List<string>();
  			foreach (XElement element in xdoc.Descendants())
  			{
  					if (element.Name == "AllSpots") {
  						nspots = (int)element.Attribute("nspots");
  					}
  					else if (element.Name == "Spot") {
  					string ID = (string)element.Attribute("ID");
  					string Name = (string)element.Attribute("name");
  					float X = (float)element.Attribute("POSITION_X");
  					float Y = (float)element.Attribute("POSITION_Y");
  					float Z = (float)element.Attribute("POSITION_Z");
  					int Frame = (int)element.Attribute("FRAME");
  					float Radius = (float)element.Attribute("RADIUS");
  					Model.G.AddNode(ID);
  					Model.AddNode(ID, Name, X, Y, Z, Frame, Radius);
  					// toRoot = ID;
  					if (spots % 2000 == 0) {
  						Debug.Log(string.Format("{0}/{1} Spots Loaded ...", spots, nspots));
  					}
  					if (spots % 100 == 0) {
  						yield return null;
  					}
  					spots += 1;
  				}
  				else if (element.Name == "Edge") {
  					string SourceID = (string)element.Attribute("SPOT_SOURCE_ID");
  					string TargetID = (string)element.Attribute("SPOT_TARGET_ID");

  					Model.G.AddEdge(SourceID, TargetID);
  					Model.AddEdge(SourceID, TargetID);

  					if (edge % 2000 == 0) {
  						Debug.Log(string.Format("{0} Edges Loaded ...", edge));
  					}
  					if (edge % 100 == 0) {
  						yield return null;
  					}
  					edge += 1;
  				}
  				else {
  					yield return null;
  				}
  			}
  			Debug.Log(string.Format("{0} Nodes Loaded ...", spots));
  			Debug.Log(string.Format("{0} Edges Loaded ...", edge));
  			Model.Ready = true;
  			Model.MakeTracks();
  		}


    }

}
