# Microscopy_VR

This is a Unity 3D app to observe Volumetric data in VR.
The goal of this application is to load volumetric data produced by LightSheet Microscopes, Confocal Z_stacks, or other form of microscopy such as MRI.
You are projected in a room, where you can load your datasets, and handle, manipulate, observe and adjust the images.

# Download
You can download the Release version from the release folder: https://gitlab.com/xqua/microscopy_vr/tree/master/releases

# Sources
Just clone this repository and open the microscopy_vr folder in Unity3D.

# Features
 - Loading volumetric data in Virtual Reality
 - Loading 4D dataset and play movies
 - Classic image processing (Intensity Contrast and Thresholding)
 - Pseudo color
 - (Soon) MultiChannel images (3 channels mapped to RGB)
 - (Less Soon) Nuclei Tracking in 4D datasets
 - (Someday) Multi user feature

# Data
The structure of a dataset is defined as such:

- DatasetFolder
  - cX_tYYYYY.raw
  - ...
  - settings.json

**DatasetFolder**: Name of the dataset
**cX_tYYYYY.raw**: Binary RAW file in 8 bit format, **cX** is the channel (example: **c0**) and **tYYYYY** is the timepoint of the dataset (example: **t00234**)
**settings.json**: Metadata file containing the volume dimensions, number of channels etc.

## Location of the Datasets
Place the datasets folders in **Microscopy_VR_Data/Textures**.

## RAW files

The RAW files must be Binary Raw File 8bits files. They can be created in many ways, the easiest being through FIJI. See below for FIJI tutorial and BigDataViewer format python scripts.

## settings.json

The settings.json files contains all the required Metadata to open the binary file. Indeed raw binary files do not contain metadatas and must be accompanied by some.

example JSON file:

```json
{
  "x":143,
  "y":164,
  "z":122,
  "channels":2,
  "timepoints":10
}
```

# How to create RAW binary 8 bit files

## FIJI
- Open your volume data as a standard XYZ Stack, then make sure to convert your images to 8 bits using **Image>Type>8-bit**.
- Save the volume data as RAW using **File>Save As>RAW Data...**
- Rename the file as: **c0t00000.raw**
- Create a new folder with the name of your dataset in the dataset folder (see above).
- Copy your raw file in that folder
- Create a new file and name it **settings.json**
- Edit it with a text editor and enter the correct information:
```json
{
  "x":SIZE_X,
  "y":SIZE_Y,
  "z":SIZE_Z,
  "channels":1,
  "timepoints":1
}
```
You can find the SIZE_X, SIZE_Y, SIZE_Z in FIJI using **Image>Show Info...** on the line: **Dimension lengths = SIZE_X,SIZE_Y,SIZE_Z**
* That's it ! Open Microscopy_VR and have fun !

## BigDataViewer XML and HDF5 files using a python script
Included in this repository is a **Utils/BDV2UnityRAW.py** containing a python script to convert BDV XML/HDF5 LightSheet data to RAW files and create the JSON automatically. Here is the documentation:

```
Usage: BDV2UnityRAW.py [options]

Options:
  -h, --help            show this help message and exit
  -i FILENAME, --input=FILENAME
                        [REQUIRED] hdf5 file path
  -o FILENAMEOUT, --output=FILENAMEOUT
                        [REQUIRED] bin file path
  -l LEVEL, --level=LEVEL
                        [REQUIRED] resolution level to extract (0: full size,
                        1: 2x downsample, 2: 4x downsample, 3: 8x downsample)
  -0 T0, --timepoint-start=T0
                        first time point to extract
  -n TN, --timepoint-end=TN
                        last time point to extract
  -C, --multichannel    Is the dataset a multichannel dataset?
  -N, --normalize       Global intensity normalization?
  -c CROP, --crop=CROP  Number of pixels to crop from the cube
  -m MIN, --min=MIN     Manually set the min. channel1,channel2
  -M MAX, --max=MAX     Manually set the max. channel1,channel2
```

example: `python3 BDV2UnityRAW.py -i PATH/TO/BDV_HDF5.h5 -o PATH/TO/OUTPUT/FOLDER -l RESOLUTION -m MIN_C1,MIN_C2 -C -M MAX_C1,MAX_C2 -N`
This will normalize the intensity accross all time points (**-N**), extract the 2 channels of the datasets (**-C**), set the minimum noise value (AKA black point) to MIN_C1/C2, and the max value (AKA saturation point) to MAX_C1/C2, with the resolution level RESOLUTION.

When you finished the creation of the dataset, just copy the folder to the Texture folder of Microscopy_VR. **Enjoy !**


# Licence

This work under the Creative Common Licence CC-BY-NC-SA
